# frozen_string_literal: true

require 'spec_helper'

describe DeepAlgo::IL::Backend::JF do
  # set log level
  DeepAlgo::IL.log.level = Logger::DEBUG
  # some instances we will test
  il_converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::Symengine, DeepAlgo::IL::Parser)
  jf_tex_converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::JF::Latex, DeepAlgo::IL::ParserJf)
  paths_to_prevent = ['.',
                      '..',
                      'clean_formula_tex_concern.rb',
                      'test_formula_with_different_states_in_properties',
                      'test_big_formula']
  TESTS_BASE_DIR_SYMENGINE = './tests_symengine'

  # parsing test
  Dir.foreach(TESTS_BASE_DIR_SYMENGINE) do |dirname|
    next if paths_to_prevent.include?(dirname) || paths_to_prevent.nil?

    path = Pathname.new("#{TESTS_BASE_DIR_SYMENGINE}/#{dirname}/formula-100.raw")
    next unless path.exist?

    it "JF convert julia form to latex  #{dirname}" do
      file = File.open("#{TESTS_BASE_DIR_SYMENGINE}/#{dirname}/formula-100.raw", 'r')
      data = file.read
      file.close

      jf = il_converter.parse(data).generate
      result = jf_tex_converter.parse(jf).generate

      file = File.open("#{TESTS_BASE_DIR_SYMENGINE}/#{dirname}/formula-100_simp.tex", 'r')
      expected = file.read
      file.close

      expect(result.gsub(/[\s\n\r]+/, '')).to eq(expected.gsub(/[\s\n\r]+/, ''))

      jf_expanded = il_converter.parse(data).generate_expanded
      file = File.open("#{TESTS_BASE_DIR_SYMENGINE}/#{dirname}/formula-100_expanded.tex", 'w')
      file.write(jf_tex_converter.parse(jf_expanded).generate)
      file.close
    end
  end
end
