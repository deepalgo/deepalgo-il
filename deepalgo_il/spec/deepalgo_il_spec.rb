# frozen_string_literal: true

require 'spec_helper'

describe DeepalgoIl do
  # set log level
  DeepAlgo::IL.log.level = Logger::DEBUG
  # some instances we will test
  parser = DeepAlgo::IL::Parser.new(DeepAlgo::IL::Backend::Generic)
  converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::Latex, DeepAlgo::IL::Parser)
  paths_to_prevent = ['.',
                      '..',
                      'clean_formula_tex_concern.rb',
                      'test_formula_with_different_states_in_properties',
                      'test_big_formula']
  TESTS_BASE_DIR = './tests'

  # parsing test
  Dir.foreach(TESTS_BASE_DIR) do |dirname|
    next if paths_to_prevent.include?(dirname) || paths_to_prevent.nil?

    path = Pathname.new("#{TESTS_BASE_DIR}/#{dirname}/formula-100.raw")
    next unless path.exist?

    it "PARSE test #{dirname}" do
      file = File.open("#{TESTS_BASE_DIR}/#{dirname}/formula-100.raw", 'r')
      result = parser.parse(file.read)
      file.close
      expect(result).to_not eq(nil)
    end
  end

  # latex backend tests
  Dir.foreach(TESTS_BASE_DIR) do |dirname|
    next if paths_to_prevent.include?(dirname) || paths_to_prevent.nil?

    path = Pathname.new("#{TESTS_BASE_DIR}/#{dirname}/formula-100.raw")
    next unless path.exist?

    it "TEXIFY test #{dirname}" do
      file = File.open("#{TESTS_BASE_DIR}/#{dirname}/formula-100.raw", 'r')
      result = converter.parse(file.read).generate
      file.close

      file = File.open("#{TESTS_BASE_DIR}/#{dirname}/formula-100_pre.tex", 'r')
      expected = file.read
      file.close

      expect(result.gsub(/[\s\n\r]+/, '')).to eq(expected.gsub(/[\s\n\r]+/, ''))
    end
  end
end
