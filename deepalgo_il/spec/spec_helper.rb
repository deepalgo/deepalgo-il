# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'deepalgo_il'
require 'pathname'
require 'logger'
require 'symengine'
