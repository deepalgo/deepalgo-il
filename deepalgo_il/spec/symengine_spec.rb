# frozen_string_literal: true

require 'spec_helper'

describe SymEngine do
  it 'symengine FunctionSymbol class exists' do
    x = SymEngine::Symbol.new('x')
    func = SymEngine::FunctionSymbol.new('f', x)
    expect(func.to_s).to eq('f(x)')
  end

  # set log level
  DeepAlgo::IL.log.level = Logger::DEBUG
  # some instances we will test
  converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::Symengine, DeepAlgo::IL::Parser)
  paths_to_prevent = ['.',
                      '..',
                      'clean_formula_tex_concern.rb',
                      'test_formula_with_different_states_in_properties',
                      'test_big_formula']
  TESTS_BASE_DIR_SYMENGINE = './tests_symengine'

  # parsing test
  Dir.foreach(TESTS_BASE_DIR_SYMENGINE) do |dirname|
    next if paths_to_prevent.include?(dirname) || paths_to_prevent.nil?

    path = Pathname.new("#{TESTS_BASE_DIR_SYMENGINE}/#{dirname}/formula-100.raw")
    next unless path.exist?

    it "SYMENGINE convert to julia expression  #{dirname}" do
      file = File.open("#{TESTS_BASE_DIR_SYMENGINE}/#{dirname}/formula-100.raw", 'r')
      result = converter.parse(file.read).generate
      file.close

      file = File.open("#{TESTS_BASE_DIR_SYMENGINE}/#{dirname}/formula-100.jf", 'r')
      expected = file.read
      file.close

      expect(result.gsub(/[\s\n\r]+/, '')).to eq(expected.gsub(/[\s\n\r]+/, ''))
    end
  end
end
