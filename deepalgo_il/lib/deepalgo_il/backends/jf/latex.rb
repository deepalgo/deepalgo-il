# frozen_string_literal: true

require_relative '../generic.rb'

module DeepAlgo
  module IL
    module Backend
      module JF
        class Latex < DeepAlgo::IL::Backend::Generic
          def initialize
            @curr_backend = nil
          end

          def generate(tree, curr_backend)
            @curr_backend = curr_backend
            latex = tree.value(@curr_backend)
            latex
          end

          def document(tree)
            tree.elements[0].value(@curr_backend)
          end

          def add_expr(tree)
            tree.elements[0].value(@curr_backend)
          end

          def assignment(tree)
            left = tree.elements[4].value(@curr_backend)
            right = tree.elements[8].value(@curr_backend)
            "#{left} \\text{equals} #{right}"
          end

          def plus_expr(tree)
            left = tree.elements[0].value(@curr_backend)
            right = tree.elements[4].value(@curr_backend)
            "#{left} + #{right}"
          end

          def minus_expr(tree)
            left = tree.elements[0].value(@curr_backend)
            right = tree.elements[4].value(@curr_backend)
            "#{left} - #{right}"
          end

          def time_expr(tree)
            left = tree.elements[0].value(@curr_backend)
            right = tree.elements[4].value(@curr_backend)
            "#{left} \\times #{right}"
          end

          def parenthesed(tree)
            "\\left( #{tree.elements[2].value(@curr_backend)} \\right)"
          end

          def colored(tree)
            color = "##{tree.elements[0].value(@curr_backend)}"
            expr = tree.elements[4].value(@curr_backend)
            "\\color{ #{color} }{ #{expr} }"
          end

          def color(tree)
            tree.elements[1].value(@curr_backend)
            # tree.text_value
          end

          def colorcode(tree)
            tree.text_value
          end

          def atom(tree)
            tree.text_value
          end

          def while_expr(tree)
            statements = tree.elements[4].value(@curr_backend)
            cond = tree.elements[8].value(@curr_backend)
            "\\text{repeat} \\left[ #{statements} \\right]#{cond}"
          end

          def while_cond_expr(tree)
            "_{ \\text{ while #{tree.elements[2].value(@curr_backend)} } }"
          end

          def text(tree)
            tree.text_value
          end

          def dotted_expr(tree)
            right = tree.elements[8].value(@curr_backend)
            left = tree.elements[4].value(@curr_backend)
            "#{left}.#{right}"
          end

          def call(tree)
            caller_name = tree.elements[0].value(@curr_backend)
            params = tree.elements[4].value(@curr_backend)
            "#{caller_name} \\left( #{params} \\right)"
          end

          def dotted_terminal(tree)
            first_expr = tree.elements[0].value(@curr_backend)
            second_expr = tree.elements[2].value(@curr_backend)
            "#{first_expr}.#{second_expr}"
          end

          def args(tree)
            # parse arg, arg, arg, ...
            to_return = ''
            tree
              .elements[1]
              &.elements
              &.each do |arg|
              to_return = "#{to_return}, \
                  #{arg.elements[3].value(@curr_backend)}"
            end
            "#{tree.elements[0].value(@curr_backend)}#{to_return}"
          end

          def tab_ref(tree)
            tab = tree.elements[4].value(@curr_backend)
            index = tree.elements[8].value(@curr_backend)
            "#{tab}_{ #{index} }"
          end

          def descriptor(tree)
            label = tree.elements[4].value(@curr_backend)
            items = tree.elements[8].value(@curr_backend)
            "\\text{ #{label} } \
                 \\left\\{\\begin{array}{l} #{items} \\\\ \\end{array}\\right."
          end

          def descriptor_items(tree)
            to_return = ''
            tree
              .elements[1]
              &.elements
              &.each do |arg|
              to_return = "#{to_return} \\\\ \
                   #{arg.elements[3].value(@curr_backend)} "
            end
            "#{tree.elements[0].value(@curr_backend)} #{to_return}"
          end

          def descriptor_item(tree)
            label = tree.elements[4].value(@curr_backend)
            assignee = tree.elements[8].value(@curr_backend)
            "\\text{ #{label} } = #{assignee}"
          end

          def div_expr(tree)
            numerator = tree.elements[0].value(@curr_backend)
            denominator = tree.elements[4].value(@curr_backend)
            "\\dfrac{ #{numerator} }{ #{denominator} }"
          end

          def unary_minus(tree)
            "-#{tree.elements[2].value(@curr_backend)}"
          end

          def power(tree)
            factor = tree.elements[0].value(@curr_backend)
            pwr = tree.elements[4].value(@curr_backend)
            "#{factor}^#{pwr}"
          end

          def identifier(tree)
            tree.text_value
          end

          def number(tree)
            tree.text_value
          end
        end
      end
    end
  end
end
