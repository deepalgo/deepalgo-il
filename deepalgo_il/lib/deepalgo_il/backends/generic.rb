# frozen_string_literal: true

module DeepAlgo
  module IL
    module Backend
      class Generic
        def generate(_tree, _curr_backend)
          raise NotImplementedError
        end

        def generate_expanded(_tree, _curr_backend)
          raise NotImplementedError
        end

        def document(_tree)
          raise NotImplementedError
        end

        def expr(_tree)
          raise NotImplementedError
        end

        def add_expr(_tree)
          raise NotImplementedError
        end

        def assignment(_tree)
          raise 'not yet implemented'
        end

        def while_expr(_tree)
          raise NotImplementedError
        end

        def while_cond_expr(_tree)
          raise NotImplementedError
        end

        def plus_expr(_tree)
          raise NotImplementedError
        end

        def minus_expr(_tree)
          raise NotImplementedError
        end

        def time_expr(_tree)
          raise NotImplementedError
        end

        def div_expr(_tree)
          raise NotImplementedError
        end

        def dotted_expr(_tree)
          raise NotImplementedError
        end

        def factor(_tree)
          raise NotImplementedError
        end

        def tab_ref(_tree)
          raise NotImplementedError
        end

        def terminal(_tree)
          raise NotImplementedError
        end

        def dotted_terminal(_tree)
          raise NotImplementedError
        end

        def atom(_tree)
          raise NotImplementedError
        end

        def descriptor(_tree)
          raise NotImplementedError
        end

        def descriptor_items(_tree)
          raise NotImplementedError
        end

        def descriptor_item(_tree)
          raise NotImplementedError
        end

        def call(_tree)
          raise NotImplementedError
        end

        def args(_tree)
          raise NotImplementedError
        end

        def parenthesed(_tree)
          raise NotImplementedError
        end

        def colored(_tree)
          raise NotImplementedError
        end

        def color(_tree)
          raise NotImplementedError
        end

        def colorcode(_tree)
          raise NotImplementedError
        end

        def identifier(_tree)
          raise NotImplementedError
        end

        def number(_tree)
          raise NotImplementedError
        end

        def unary_minus(_tree)
          NotImplementedError
        end

        def text(_tree)
          raise NotImplementedError
        end

        def power(_tree)
          raise NotImplementedError
        end

        def number(_tree)
          raise NotImplementedError
        end
      end
    end
  end
end
