# frozen_string_literal: true

require_relative './generic.rb'
require_relative '../parser/parser'
require_relative '../converter'
require 'symengine'

module DeepAlgo
  module IL
    module Backend
      class Symengine < Generic
        def initialize
          @curr_backend = nil
          @jf = nil
        end

        def generate(tree, curr_backend)
          @curr_backend = curr_backend
          symengine_jf = tree.value(@curr_backend)
          @jf = eval("(#{symengine_jf}).to_s")
          @jf
        end

        def generate_expanded(tree, curr_backend)
          @curr_backend = curr_backend
          symengine_jf = tree.value(@curr_backend)
          @jf = eval("(#{symengine_jf}).expand.to_s")
          @jf
        end

        def document(tree)
          tree.elements[0].value(@curr_backend)
        end

        def add_expr(tree)
          tree.elements[0].value(@curr_backend)
        end

        def assignment(tree)
          left = tree.elements[0].value(@curr_backend)
          right = tree.elements[4].value(@curr_backend)
          "SymEngine::FunctionSymbol.new('@EQ@', #{left}, #{right})"
        end

        def plus_expr(tree)
          left = tree.elements[0].value(@curr_backend)
          right = tree.elements[4].value(@curr_backend)
          "#{left} + #{right}"
        end

        def minus_expr(tree)
          left = tree.elements[0].value(@curr_backend)
          right = tree.elements[4].value(@curr_backend)
          "#{left} - #{right}"
        end

        def time_expr(tree)
          left = tree.elements[0].value(@curr_backend)
          right = tree.elements[4].value(@curr_backend)
          "#{left} * #{right}"
        end

        def parenthesed(tree)
          "( #{tree.elements[2].value(@curr_backend)} )"
        end

        def colored(tree)
          color = tree.elements[0].text_value # @color-abcdef@
          expr = tree.elements[4].value(@curr_backend)
          "SymEngine::FunctionSymbol.new('#{color}', #{expr} )"
        end

        def color(tree)
          tree.elements[1].value(@curr_backend)
          # tree.text_value
        end

        def colorcode(tree)
          tree.text_value
        end

        def atom(tree)
          "SymEngine::Symbol.new('#{tree.text_value}')"
        end

        def while_expr(tree)
          statements = tree.elements[4].value(@curr_backend)
          cond = tree.elements[8].value(@curr_backend)
          "SymEngine::FunctionSymbol.new('@REPEAT@', #{statements}, #{cond})"
        end

        def while_cond_expr(tree)
          "SymEngine::Symbol.new('while #{tree.elements[4].value(@curr_backend)}')"
        end

        def text(tree)
          tree.text_value
        end

        def dotted_expr(tree)
          right = ''
          left = tree.elements[0].value(@curr_backend)
          if !tree.elements[1].nil? && tree.elements[1].text_value != ''
            right = tree
                    .elements[1]
                    .elements[3].value(@curr_backend)
            return "SymEngine::FunctionSymbol.new('@REF@', #{left}, #{right})"
          end

          left
        end

        def call(tree)
          caller_name = tree.elements[0].value(@curr_backend)
          params = tree.elements[4].value(@curr_backend)
          "#{caller_name} \\left( #{params} \\right)"
          "SymEngine::FunctionSymbol.new(#{caller_name}.to_s, #{params})"
        end

        def dotted_terminal(tree)
          first_expr = tree.elements[0].value(@curr_backend)
          second_expr = tree.elements[2].value(@curr_backend)
          "SymEngine::FunctionSymbol.new('@REF@', #{first_expr}, #{second_expr})"
        end

        def args(tree)
          # parse arg, arg, arg, ...
          to_return = ''
          tree
            .elements[1]
            &.elements
            &.each do |arg|
            to_return = "#{to_return}, \
              #{arg.elements[3].value(@curr_backend)}"
          end
          "#{tree.elements[0].value(@curr_backend)}#{to_return}"
        end

        def tab_ref(tree)
          tab = tree.elements[0].value(@curr_backend)
          index = tree.elements[6].value(@curr_backend)
          "SymEngine::FunctionSymbol.new('@REFA@', #{tab}, #{index})"
        end

        def descriptor(tree)
          label = tree.elements[2].value(@curr_backend)
          items = tree.elements[8].value(@curr_backend)
          desc = "SymEngine::FunctionSymbol.new('@DESC@', SymEngine::Symbol.new('#{label}'), #{items})"
          if !tree.elements[11].nil? && tree.elements[11].text_value != ''
            ref = tree.elements[11].elements[3].value(@curr_backend)
            return "SymEngine::FunctionSymbol.new('@REF@', #{desc} , #{ref} )"
          end

          desc
        end

        def descriptor_items(tree)
          to_return = ''
          tree
            .elements[1]
            &.elements
            &.each do |arg|
            to_return = "#{to_return}, \
               #{arg.elements[1].value(@curr_backend)} "
          end
          "#{tree.elements[0].value(@curr_backend)} #{to_return}"
        end

        def descriptor_item(tree)
          label = tree.elements[4].value(@curr_backend)
          assignee = tree
                     .elements[10]
                     .elements[0]
                     .value(@curr_backend)
          "SymEngine::FunctionSymbol.new('@EQ@', SymEngine::Symbol.new('#{label}'), #{assignee})"
        end

        def div_expr(tree)
          numerator = tree.elements[6].value(@curr_backend)
          denominator = tree.elements[14].value(@curr_backend)
          "(#{numerator})/(#{denominator})"
        end

        def unary_minus(tree)
          "-#{tree.elements[2].value(@curr_backend)}"
        end

        def identifier(tree)
          tree.text_value
        end
      end
    end
  end
  end
