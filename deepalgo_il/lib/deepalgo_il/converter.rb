# frozen_string_literal: true

require_relative 'parser/parser'

module DeepAlgo
  module IL
    class Converter
      def initialize(backend_type, parser_type)
        @curr_backend = backend_type.new
        @parser = parser_type.new(@curr_backend)
        @tree = nil
      end

      def parse(code)
        @tree = @parser.parse(code)
        self
      end

      def generate
        return nil if @tree.nil? || @curr_backend.nil?

        @curr_backend.generate(@tree, @curr_backend)
      end

      def generate_expanded
        return nil if @tree.nil? || @curr_backend.nil?

        @curr_backend.generate_expanded(@tree, @curr_backend)
      end
    end
  end
  end
