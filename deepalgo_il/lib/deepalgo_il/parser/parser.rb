# frozen_string_literal: true

require 'treetop'
require_relative '../backends'
require_relative '../logging'

module DeepAlgo
  module IL
    class GlobalParser
      def parse(input)
        data = input.gsub(/(?:#.+?#)/, '')
        tree = nil
        begin
          tree = @parser.parse(data)
        rescue StandardError
          DeepAlgo::IL.log.debug('Error when parsing the file')
          DeepAlgo::IL.log.debug(input.to_s)
        end
        DeepAlgo::IL.log.debug(@parser.failure_reason) if tree.nil?
        tree
      end
    end

    class Parser < GlobalParser
      BASE_PATH = __dir__

      def initialize(curr_backend)
        @curr_backend = curr_backend
        Treetop.load(File.join(BASE_PATH, 'il_parser.treetop'))
        @parser = IlParser.new
      end
    end

    class ParserJf < GlobalParser
      BASE_PATH = __dir__

      def initialize(curr_backend)
        @curr_backend = curr_backend
        Treetop.load(File.join(BASE_PATH, 'jf_parser.treetop'))
        @parser = JfParser.new
      end
    end
  end
end
