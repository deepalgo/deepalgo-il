# frozen_string_literal: true

require 'logger'

module DeepAlgo
  module IL
    @log ||= Logger.new(STDOUT)
    @log.level = ENV.fetch('LOG_LEVEL', Logger::INFO)
    class << self
      attr_reader :log
    end
  end
end
