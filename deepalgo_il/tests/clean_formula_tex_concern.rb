# frozen_string_literal: true

class String
  def texescape
    front = '\text{'
    body = gsub(/_/, '\_')
           .gsub(/%/, '\%')
    back = '}'
    front + body + back
  end

  def texify
    gsub(/@RETURN@/, '')
      .gsub(/@LPAREN@/, "\\left(\s")
      .gsub(/@RPAREN@/, "\\right)\s")
      .gsub(/@GRIGHT@/, '\\right')
      .gsub(/@GLEFT@/, '\\left')
      .gsub(/@RBRACK@/, '\\right]')
      .gsub(/@LBRACK@/, '\\left[')
      .gsub(/@\+@/, "\s+\s")
      .gsub(/@x@/, "\s\\times\s")
      .gsub(/@-@/, "\s-\s")
      .gsub(/@DIV@/, "\s\\dfrac")
      .gsub(%r{@/DIV@}, '')
      .gsub(/@NUM@/, '')
      .gsub(/@DENO@/, '')
      .gsub(/@REF@/, '.')
      .gsub(/@REFA@/, '_')
      .gsub(/@EOFROW@/, '\\\\\ ' + "\n ")
      .gsub(/@UNDERSCRIPT@/, '_')
      .gsub(/@EQ@/, '\text{ equals } ')
      .gsub(/@AND@/, '\text{ and }')
      .gsub(/@OR@/, '\text{ or }')
      .handle_describers
  end

  def handle_describers
    r = gsub(/@DESCSTART@/, '\left\{\begin{array}{l}')
        .gsub(/@DESCITEMSTART@/, ' ')
        .gsub(/@DESCITEMEND@/, ' \\\\\\')
        .gsub(/@DESCEND@/, '\end{array}\right.')

    r.dup.scan(/(@TSTART@)([^@]+)(@TEND@)/) do |m|
      if m[1].start_with?('\\text{')
        next r.sub!("#{m[0]}#{m[1]}#{m[2]}", (m[1]).to_s)
      end

      r.sub!("#{m[0]}#{m[1]}#{m[2]}", "\\text{ #{m[1]} }")
    end
    r
  end

  def texcolorize
    gsub(/@COLOR\-(\w+)@/, '\color{#\1}')
  end
end

# require 'symengine'
require 'digest/md5'
module DeepAlgo
  module Audit
    module Engine
      module Api
        module V0
          module CleanFormulaTexConcern
            PAREN_OPERATORS_REG = /(?:-|\\times)\s*$/.freeze
            # @param what[String]
            # @param formula[String]
            # @param subst[String]
            # @param
            def try_with_or_without_pred_char(sub, candidate, _nochar = '', bchar = '(\W|^)', echar = '(\W|$)')
              subst = sub.by.ope.oid + sub.by_text + sub.by.ope.oid
              reg = build_regex(sub.what, bchar: bchar, echar: echar)
              # TODO: insure non char caracters are followed by a space
              handle_parenthesis!(sub.what, subst)
              sub_formula = candidate.gsub(reg, '\1' + subst + '\2')
              # if sub_formula == formula
              #   L.formula_solver__warning { "the formula #{formula} wasn't substituted by #{reg}. We try without separator...this may lead to bad substitution" }
              #   reg = build_regex(what, bchar: nochar, echar: nochar)
              #   sub_formula = formula.gsub(reg, subst)
              # end
              # TODO: revert non char preceeded by space
              sub_formula
            end

            def handle_parenthesis!(what, subst)
              subst.sub!(subst, "@LPAREN@ #{subst} @RPAREN@") if
                what.match(/^#Call[^#]+?#Method/) || (
                  !what.start_with?('#Call') &&
                  what.match(/@([-+x]|DIV|EQ|AND|OR)@/))
            end

            def build_regex(what, bchar: nil, echar: nil)
              reg = /#{bchar}#{Regexp.quote(what)}#{echar}/
              # puts reg
              # reg
            end

            # @param what[String]
            # @param formula[String]
            # @param subst[String]
            def apply_regexp(sub, candidate)
              try_with_or_without_pred_char(sub, candidate)
            end

            PALETTE = ['#E84F34',
                       '#BF432A',
                       '#222D2C',
                       '#00AAC0',
                       '#95C11F',
                       '#A11876',
                       '#1a96a7',
                       '#F9C100',
                       '#a271ab',
                       '#75af95'].freeze
            def text2color(string)
              # PALETTE[(string.size % PALETTE.size)]
              '#' + Digest::MD5.hexdigest(string)[0..5]
            end

            # def colorized_tex(tex_expression, string_to_use_for_color = tex_expression)
            #   "@#{text2color(tex_expression, string_to_use_for_color)}@ #{tex_expression} @#{text2color(tex_expression, string_to_use_for_color)}@"
            # end

            # def apply_tex_color!(formula)
            #   formula.gsub!(/@(#\w+)@\s(.+?)\s@(#\w+)@/, '\textcolor{' + '\1' + '}{\mathbf{' + '\2' + '}}')
            #   raise "#{formula} was not colored !!!" if formula.match(/@#\w+@/)

            #   formula
            # end

            # def colorized_tex!(tex_expression, string_to_use_for_color = tex_expression)
            #   res = colorized_tex(tex_expression, string_to_use_for_color)
            #   tex_expression.sub!(/.*/, res)
            #   tex_expression
            # end

            def clean_formula_tex(formula)
              formula.gsub(/(?:#.+?#)/, '').texify
              # .gsub(/\w+@REF@/, '')
              # .gsub(/\".*?\"@REF@(\".*?\")/, '\1')
              # .gsub(/@RETURN@/, '')
              # .gsub(/(\w+|\))@REFA@/, '\1_')
              # .gsub(/@REFA@/, '\bigg\rvert_')
              # .gsub(/@REF@/, '')
              # .gsub(/\*\*/, '^')
              # .gsub(/\\left\(\\right\)/, '')
              # .gsub(/\s+/, "\s")
            end
          end
        end
      end
    end
  end
end

Dir.foreach('.') do |dirname|
  if dirname == '.' || dirname == '..' || dirname == 'clean_formula_tex_concern.rb'
    next
  end

  puts "working on #{dirname}"

  Dir.foreach("./#{dirname}") do |filename|
    next if filename == '.' || filename == '..' || filename.nil?
    next if filename.split('.')[1] != 'raw'

    file = File.open("./#{dirname}/#{filename}", 'r')
    formula = file.read
    inter = formula.gsub(/(?:#.+?#)/, '')
    fileinter = File.open("./#{dirname}/#{filename.split('.')[0]}.inter", 'w')
    fileinter.write(inter)
    res = inter.texify.texcolorize
    newfile = File.open("./#{dirname}/#{filename.split('.')[0]}_pre.tex", 'w')
    newfile.write(res)
    fileinter.close
    newfile.close
    file.close
  end
end
