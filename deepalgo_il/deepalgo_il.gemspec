# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'deepalgo_il/version'

Gem::Specification.new do |spec|
  spec.name          = 'deepalgo_il'
  spec.version       = DeepalgoIl::VERSION
  spec.authors       = ['Valderane Potong Mboucheko']
  spec.email         = ['valderane.potongmboucheko@margo-group.com']

  spec.summary       = 'Little formula traductor from IL to a backend of your choice (Latex for exemple)'
  spec.description   = 'This gem provides some functions to convert IL into a backend we want.'
  # spec.homepage      = "TODO: Put your gem's website or public repo URL here."
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.1'
  spec.add_development_dependency 'logger'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'symengine', '~> 0.1.0'
  spec.add_development_dependency 'treetop', '~> 1.6'
end
