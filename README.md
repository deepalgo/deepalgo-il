# DeepalgoIl

DeepalgoIl is a little module that aim to convert  *IL* (Intermediate Language which represents mathematical expressions used by [DeepAlgo](https://www.deepalgo.com/)) into a certain human readable form (it can be javascript, html, latex, etc...)
The goal is to have a bunch of *Backends* in which the *IL* can be represented. More, we should to be able to implement a new backend rapidly.

* What is IL ?
DeepAlgo is a web app that analyses source code and represent it into a mathematical expression. For example if we have this simple code in Java:
```java
class Test {
    ...
    public int square(a) {
        return a*a
    }

    public int function(x, y) {
        return this.square(x + y) / this.square(x)
    }
}

```
DeepAlgo can produce something like:
```
given x and y

Test.function = ((x + y)*(x + y)) / ((x*x))
```
To produce that output, DeepAlgo substitute functions with the help of a call graph. This operation first produce an expression in a specific form, we call it IL (Intermediate Language). In IL, some  parts of the expression are represented in a specific form in order to not be conflicted with the code we analyse. For example, the IL corresponding to the expression above can be:
```
@DIV@ @NUM@ @LPAREN@ @LPAREN@ Parameter:x @+@ Parameter:y @RPAREN@ @x@ @LPAREN@ Parameter:x @+@ Parameter:y @RPAREN@ @DENOM@ @LPAREN@ @LPAREN@ Parameter:x + Parameter:x @RPAREN@ @RPAREN@ @/DIV@
```
- The first goal of this module is to convert that IL into a human readable form such as Latex. For example a Latex for of the expression above can be:
```tex
\frac{\left( \left( Parameter:x + Parameter:y \right)  \times  \left( Parameter:x + Parameter:y \right) \right) }{ \left( \left( Parameter:x \times Parameter:x \right) \right) }
```
<img src="./readme_images/readme_formula.png">

- The second goal is to simplify the expression, for example by removing the extra parenthesis or simplify fractions, then produce an output in a backend. Do do that, we use a library called symEngine, which help us to first go from IL to JF (Juila form) expression (the JF expression is already simplified thanks to symEngine), then we transform JF to a backend we want. For example a simplified JF of the above IL can be:

```
(Parameter:x + Parameter:y)**2 / Parameter:x
```
in a Latex backend:

```tex
\frac{ { \left( Parameter:x + Parameter:y \right) }^2 }{ Parameter:x }
```
<img src="./readme_images/readme_formula_simp.png">

We will explain how these goals can be achieved in the following lines.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'deepalgo_il'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install deepalgo_il

## Usage

To use this library, you have to import it in your code
```
import 'deepalgo_il'
```
The are four usecases of the gem:


### If you want to convert IL to a Backend (Latex for example)
You need to nstanciate a converter by specifying the backend you want to use and the IL parser. For example to convert to latex, you can instanciate a converter like that:

```ruby
# DeepAlgo::IL::Parser is the parser to use if you want to parse IL
converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::Latex, DeepAlgo::IL::Parser)
```
Assuming you have your IL expression in a variable *data*, to convert *data* to latex you can do:
```
data_tex = converver.parse(data).generate
```
Your *data* string will be converted to latex by *converter* and the latex result will be stored in *data_tex*

The whole code:

```ruby
require 'deepalgo_il'

converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::Latex, DeepAlgo::IL::Parser)
# ...
# some code where you have defined a string named "data" that contains an IL expression
# ...
data_tex = converver.parse(data).generate
```
Available backends:
* Latex (DeepAlgo::IL::Backend::Latex)
* SymEngine (DeepAlgo::IL::Backend::SymEngine). This backend produces JF from IL

### If you want to convert IL into a julia form (JF)
Reproduce the steps above, with the SymEngine backend

### If you want to convert JF into a backend
You need to nstanciate a converter by specifying the backend you want to use and the JF parser. For example to convert to latex, you can instanciate a converter like that:

```ruby
# DeepAlgo::IL::ParserJf is the parser to use if you want to parse JF
# Every backend for JF is under the namespace DeepAlgo::IL::Backend::JF
converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::JF::Latex, DeepAlgo::IL::ParserJf)
```
Assuming you have your jf expression in a variable *data*, to convert *data* to latex you can do:
```
data_tex = converver.parse(data).generate
```
Your *data* string will be converted to latex by *converter* and the latex result will be stored in *data_tex*

The whole code:

```ruby
require 'deepalgo_il'

converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::JF::Latex,  DeepAlgo::IL::ParserJf)
# ...
# some code where you have defined a string named "data" that contains an JF expression
# ...
data_tex = converver.parse(data).generate
```
Available backends:
* Latex (DeepAlgo::IL::Backend::JF::Latex) (note that this backend differs from DeepAlgo::IL::Backend::Latex !)

### If you want to convert IL into a Backend with simplification
Simply first convert IL into JF then JF into backend

For example:
```ruby
require 'deepalgo_il'
# we use Symengine backend to convert IL to JF
il_converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::Symngine,  DeepAlgo::IL::Parser)
#  we use Symengine backend to convert IL to JF
jf_converter = DeepAlgo::IL::Converter.new(DeepAlgo::IL::Backend::JF::Latex,  DeepAlgo::IL::ParserJf)
# ....
# some code where a variable 'data' is initialized and contains an IL expression
# ....
data_jf = il_converter.parse(data).generate

data_tex = jf_converter.parse(data_jf).generate  # there we have our simplified latex

```
## Development

After checking out the repo, you will first first need to install our patched version of symEngine gem. To do so :
```bash
# clone the latest symengine c++ lib
apt-get update -y && apt-get install -y cmake libgmp-dev
git clone https://github.com/symengine/symengine.git
cd symengine
# install the c++ lib
mkdir build && cd build
cmake .. && make
make install
# install the symengine gem
git clone https://github.com/symengine/symengine.rb.git && cd symengine.rb
patch -p1 < /path/to/deepalgo_il/root/directory/complex.patch
gem install bundler -v 1.7 && bundle install
gem build symengine.gemspec # this will create symengine-0.1.0.gem here, note this path, you will use it later

```
In the deepalgo_il root repository, do these steps:
```bash
cd deepalgo_il # note that deepalgo_il is in deepalgo_il
gem install bundler -v 2.1 # uninstall bundler 1.7 if necessary
mkdir -p vendor/cache
cp -r /path/to/symengine-0.1.0.gem vendor/cache

# install the deepalgo_il dependencies
bundle install --no-cache
```

Run tests
```bash
bundle exec rspec spec
```

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Credits

[SymEngine](https://github.com/symengine/symengine): This awesome library helped us for expressions simplification

